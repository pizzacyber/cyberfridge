We, as a modern society, should strive to cyber all of our daily life.
That being said, We are planning to give the world a present.
Its very own- open source, cyber fridge.

The initial demo of the project should be released sometime in the beginning of February.
The platform: an Android application written using a Python library called Kivy.


**Basic features:** (which will be in the demo):

\* A list of the contents of the fridge, ordered by name and expiration date.

\* An ability to add / remove items from the list at any time.

\* The app will notify the user when a product is about to expire using push 
notifications.

\* Simple GUI which will allow easy interaction with the app.

\* Notifying at a set time what items are missing from the fridge.


**Bonus features:**
\* Using the device's camera (or a barcode reader) to log the product into the list automatically

\* Nice GUI

\* Calender view


*GLHF*