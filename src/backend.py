import datetime
from kivy.storage.dictstore import DictStore

''' backend.py {MODULE_BACKEND}
Description:
    This module represents the backend of the cyberfridge project.
    It handles the management of data, calculation, and algorithms.
    This module serves the frontend module {MODULE_FRONTEND}.
Objects:
    class Manager {BACKEND_MANAGER}
        The main class to serve the frontend. In charge of the data.
    function convert_color {BACKEND_CONV-COLOR}
        Converts colors from RGB/RGBA formats to kivy format
    Exceptions {BACKEND_EXCEPTIONS}
        invalid_format: Raised when arguments are not in the correct format.
        product_missing: Raised when an action is requested on a product that does not exist
        date_expired: Raise when an expired product is requested to be inserted.
'''


''' class Manager {BACKEND_MANAGER}
Descirption:
    This class is the main class of the module.
    It holds the contents of the fridge, and it's the class's job  to manage it.
    The frontend will use it's functions to add, remove, and list the contents of the fridge.
    It also manages a database to keep the data between sessions.
Functions:
    __init__ {BACKEND_MANAGER_INIT}
        The constructor of the Manager object
    __del__ {BACKEND_MANAGER_DEL}
        The destructor of the Manager object
    add_product {BACKEND_MANAGER_ADD-PROD}
        Adds a product to the fridge
    remove_product {BACKEND_MANAGER_RM-PROD}
        Removes a product from the fridge
    list_products {BACKEND_MANAGER_PRODS-LIST}
        Lists of all the product names
    list_product {BACKEND_MANAGER_PROD-LIST}
        Lists all instances of a product
    expires_today {BACKEND_MANAGER_EXP-TODAY}
        Lists all products which expires today
    expired {BACKEND_MANAGER_EXPIRED}
        Lists all products which have already expired
'''
class Manager:

    ''' __init__ {BACKEND_MANAGER_INIT}
    Description:
        The constructor of the class.
        Restores the data on products from the database
    Args: None
    Returns: None
    Raises: None
    '''
    def __init__(self):
        self.store = DictStore('products.data')
        if self.store.exists('products'):
            self.products = self.store['products']
        else:
            self.products = {} 

    ''' __del__ {BACKEND_MANAGER_DEL}
    Description:
        The destructor of the class.
        Stores the data on products in the database
    Args: None
    Returns: None
    Raises: None
    '''
    def __del__(self):
        self.store['products'] = self.products

    ''' add_product {BACKEND_MANAGER_ADD-PROD}
    Description:
        Adds information about a product (name, expiration date) to a dictionary.
        Can be used in 2 different ways, using a product string, or seperate name and date.
        See 'Args' section.
    Args:
        product (string) :
            Could be used in 2 ways:
            - product string mode e.g 'Banana 12/07/17'
                This will parse the string.
                it will use the first term as the product name, and the second as the date.
            - product name e.g 'Banana'
                This will work in conjunction with the second argument.
        expires (string) :
            defaults to be None, so that 'product string' mode will work.
            Using the function in mode #2 (as seen in the first argument),
            Will require the user to enter a string representing the date in this argument.
            see 'Examples' section
        #Note:
            The date must be in dd/mm/yy or dd/mm/yyyy
            e.g : 12/07/17 or 12/07/2017
    Returns: None
    Raises:
        invalid format:
            - If used in 'product string' mode, but the string could not be parsed correctly
            object raised: 'product' argument
            - If date is not in the correct format (see Note above)
            object raised: tuple('product' arg, 'expires' arg)
        date_expired:
            If an attempt is made to add a product with an already-expired expiration date.
            object raised: tuple('product' arg, 'expires' arg)
    Example:
        >> m = backend.Manager()
        >> m.add_product('Banana 12/07/17') #product string mode
        >> m.add_product('Apple', '12/07/2017') #split mode
        >> m.add_product('Banana 12/07/2010') # raises date_expired
        >> m.add_product('Banana 12/07/17 asd') # raises invalid_format
        >> m.add_product('Banana','12/07/12/a') # raises invalid_format
    '''
    def add_product(self,product,expires=None):
        if expires == None:
            print '[*] Recieved full product string'
            print '[$] Trying to split product string into name and date'
            product_info = product.split(' ')
            if len(product_info) != 2:
                print '[!] Bad product string format'
                raise invalid_format(product)
            else:
                print '[#] Format correct.'
                product,expires = product_info
        print '[*] Adding product ' + product + ' which expires at ' + expires
        print '[$] Testing full format'
        try:
            d = datetime.datetime.strptime(expires,'%d/%m/%Y').date()
            print '[#] Date is in full format'
        except:
            print '[!] Date is not in full format'
            print '[$] Testing short format'
            try:
                d = datetime.datetime.strptime(expires,'%d/%m/%y').date()
                print '[#] Date is in short format'
            except:
                print "[!] 'expires' variable is not in correct date format."
                raise invalid_format((product,expires))
        if d < datetime.date.today():
            print '[!] Date has already expired'
            raise date_expired((product,expires))
        if product not in self.products.keys():
            self.products[product] = [d]
        else:
            self.products[product].append(d)

    ''' remove_product {BACKEND_MANAGER_RM-PROD}
    Description:
        Removes an instance of a product of choice from the dictionary.
    Args:
        product (string) : The name of the product to remove
        expires (string) : 
            The instance of the object you wish to delete 
            (these are identified by their expiration date)
    Returns: None
    Raises:
        product_missing:
            - If the requested product name is not in the dictionary
            object raised: tuple('product' arg, 'expired' arg)
            - If No instance of 'expires' arg found for requested product
            object raised: tuple('product' arg, 'expired' arg)
    Example:
        >> m = backend.Manager()
        >> m.add_product('Banana 12/07/17')
        >> m.add_product('Banana 13/07/17')
        >> m.list_product('Banana')
        ['12/07/17','13/07/17']
        >> m.remove_product('Banana','13/07/17')
        >> m.list_product('Banana')
        ['12/07/17']
        >> m.remove_product('Banana','13/07/17') # raises product_missing
        >> m.remove_product('Orange','12/07/17') # raises product_missing
    '''
    def remove_product(self,product,expires):
        print '[*] Removing product ' + product + ' which expires at ' + expires
        print '[$] Testing if product is in system'
        if product in self.products.keys():
            if expires in self.products[product]:
                print '[#] Product found, deleting.'
                self.products.remove(expires)
            else:
                print '[!] No such expiration date for that product'
                raise product_missing((product,expires))
        else:
            print '[!] Product not found in dictionary'
            raise product_missing((product,expires))

    ''' list_products {BACKEND_MANAGER_PRODS-LIST}
    Description: Lists the names of all the product in the dictionary
    Args: None
    Returns: list(string) : A list of all product names in the fridge.
    Raises: None
    Example:
        >> m = backend.Manager()
        >> m.add_product('Banana '12/07/17')
        >> m.add_product('Orange','12/07/17')
        >> m.add_product('Banana','13/07/17')
        >> m.list_products()
        ['Banana','Orange']
    '''
    def list_products(self):
        print '[*] Listing all products'
        return self.products.keys()

    '''  list_product {BACKEND_MANAGER_PROD-LIST}
    Description: Lists all instances (expiration dates) of a requested product.
    Args:
        product (string) : The product name requested
    Returns:
        list(string) : A list of all the instances (dates) of the requested product
    Raises:
        product_missing : If the requested product name does not exist in the dictionary.
        object raised: 'product' argument
    Example:
        >> m = backend.Manager()
        >> m.add_product('Banana','12/07/17')
        >> m.add_product('Banana','13/07/17')
        >> m.list_product('Banana')
        ['12/07/17','13/07/17']
        >> m.list_product('Orange') # raises product_missing
    '''
    def list_product(self,product):
        print '[*] Listing entries of product ' + product
        if product not in self.products.keys():
            print '[!] Products ' + product + ' does not exist in the system'
            raise product_missing(product)
        else:
            ret = map(lambda x: x.strftime('%d/%m/%y'), self.products[product])
            return ret

    ''' expires_today {BACKEND_MANAGER_EXP-TODAY}
    Description:
        Lists all the products which expire today
        The format is a tuple of the product name and the expiration date,
        so that if multiple instances of the same product are expiring today,
        the function will return all those instances.
    Args: None
    Returns:
        list(tuple(string,string)) : A list of tuples.
        Each tuple holds the name of the product expiring, and it's expiration date.
    Raises: None
    Example:
        # today is 12/07/17
        >> m = backend.Manager()
        >> m.add_product('Banana','12/07/17')
        >> m.add_product('Banana 12/07/17')
        >> m.add_product('Banana 13/07/17')
        >> m.add_product('Orange','12/07/17')
        >> m.expires_today()
        [('Banana','12/07/17'),('Banana','12/07/17'),('Orange','12/07/17')]
    '''
    def expires_today(self):
        today = []
        for product in self.products.keys():
            for date in self.products[product]:
                if date == datetime.date.today():
                    today.append((product,date.strftime('%d/%m/%y')))
        return today

    ''' expired {BACKEND_MANAGER_EXPIRED}
    Description:
        Lists all the products which have already expired
        The format is a tuple of the product name and the expiration date,
        so that if multiple instances of the same product have expired
        the function will return all those instances.
    Args: None
    Returns:
        list(tuple(string,string)) : A list of tuples.
        Each tuple holds the name of the product expired, and it's expiration date.
    Raises: None
    Example:
        # today is 12/07/17
        >> m = backend.Manager()
        >> m.add_product('Banana','11/07/17')
        >> m.add_product('Banana 10/07/17')
        >> m.add_product('Banana 13/07/17')
        >> m.add_product('Orange','9/07/17')
        >> m.expires_today()
        [('Banana','10/07/17'),('Banana','11/07/17'),('Orange','9/07/17')]
    '''
    def expired(self):
        expired = []
        for product in self.products.keys():
            for date in self.products[product]:
                if date < datetime.date.today():
                    expired.append((product,date.strftime('%d/%m/%y')))
        return expired

''' convert_color {BACKEND_CONV-COLOR}
Description: Converts a color in either RGB or RGBA format into kivy color format
Args:
    t (tuple(num,num,num) or tuple(num,num,num,num) : Color in RGB or RGBA format.
    values must be in the valid format ( 0 <= i <= 255 )
Returns:
    tuple(num,num,num) : RGB in kivy format (values from 0 to 1)
    tuple(num,num,num,num) : RGBA in kivy format.
        note that the alpha values are reversed in kivy format (255-RGB = 0-kivy)
Raises:
    invalid_format:
        - argument is not iterable
        - argument is not of length 3 or 4
        - values are not between 0 and 255
'''
def convert_color(t):
    try:
        if len(t) not in (3,4):
            raise invalid_format(t)
    except TypeError:
        raise invalid_format(t)
    for channel in t:
        if channel < 0 or channel > 255:
            raise invalid_format(t)
    kivy_format = map(lambda x:(float(x)/255.0),t)
    if len(t) == 4:
        kivy_format[3] = 1 - kivy_format[3] #Alpha in kivy and rgba are reversed
    return tuple(t)

''' is_english {BACKEND_IS-ENG}
Description: Checks if a string is in english
Args:
    s (str) : string to check if in english
Returns:
    bool : True if in english and False otherwise
Raises: None
'''
def is_english(s):
    return s.isalnum()

''' {BACKEND_EXCEPTIONS} '''
class invalid_format(Exception):
    pass
class product_missing(Exception):
    pass
class date_expired(Exception):
    pass
