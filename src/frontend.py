#!/usr/bin/python2.7

import time
import random

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.tabbedpanel import TabbedPanelHeader
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.uix.popup import Popup
from kivy.uix.listview import ListView
from kivy.adapters.simplelistadapter import SimpleListAdapter
from kivy.uix.actionbar import ActionBar
from kivy.uix.actionbar import ActionView
from kivy.uix.actionbar import ActionItem 
from kivy.clock import Clock
from kivy.utils import get_random_color

import backend

class MyButton(Button, ActionItem):
    pass

class MyLable(Label):
    pass


class StatusBar(ActionBar):
    pass

class MyClock(Label):
    def update(self, *args):
        self.text = time.asctime()
        self.color=get_random_color()

class MyLayout(FloatLayout):
    pass


class MainWidg(Widget):
    def __init__(self, **kwargs):        
        super(MainWidg, self).__init__(**kwargs)
        self.m = backend.Manager()
        Clock.schedule_interval(self.ids.clock.update, 1)

    #Some handlers:
    def add_on_enter(self, value):
        tmp= value.text
        print tmp
        value.text = ""
        try:
            self.m.add_product(tmp)
        except backend.invalid_format:
            #create a popup
            pass
        except backend.date_expired:
            #same
            pass


    def show_items(self):
        data = [i[::-1] for i in self.m.list_products()]
        adapt = SimpleListAdapter(data=data, cls=MyLable, halign='left',
                valign='left',font_name='DejaVuSans.ttf')
        list_view = ListView(adapter=adapt)
        popup= Popup(title= 'Item list:', content=list_view,size_hint=(None,None),
                size=(300,600))
        popup.open()

    def add_item(self):
        lay = FloatLayout()

        txt = TextInput(size_hint=(1,1), pos_hint={'right':1,'center_y':0.7},
            multiline=False, font_name='DejaVuSans.ttf')
        
        txt.bind(on_text_validate=self.add_on_enter)
        lay.add_widget(txt)
        pop1 = Popup(title='Input', content=lay, size_hint=(0.6,0.4))
        pop1.open()
    
    def show_expired(self):
        data = self.m.expired()
        adapt = SimpleListAdapter(data=data, cls=Label, haligh='left', valign='left')
        list_view = ListView(adatper=adapt)
        popup = Popup(title='Expired products', content=list_view, size_hint=(None,None), size=(300,600))
        popup.open()

class CyberFridge(App):
	def build(self):
		return MainWidg()

def main():
	Builder.load_file('gui.kv')
	CyberFridge().run()

if __name__=="__main__":
    main()
